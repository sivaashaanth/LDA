I have a schema like this:
Alerts: [
     {
         _id: ...,
         Tags: ["a", "b"]
     }
]

I am trying to do a search for elements inside the second array, but mongo aparently has not support to a nested command.
This command works:
query = query.Where(x => x.Tags.Any(tag => myFilter.Tags.Contains(tag)));

But I need to do this using case insensitive. I have tried some alternatives like:
query = query.Where(a => a.Tags.Any(tag => myFilter.Tags.Contains(tag, StringComparer.OrdinalIgnoreCase)));

query = query.Where(a => a.Tags.Any(tag => myFilter.Tags.Any(t => t.Equals(tag, StringComparison.OrdinalIgnoreCase))));

But I get a similar error:
ArgumentException: Unsupported filter: Any(value(System.Collections.Generic.List`1[System.String]).Where({document}.Equals({document}, OrdinalIgnoreCase))).

I also have tried to use Contains, Any, Where, Exists. None of them worked.
How can I do that search with case insensitive?
