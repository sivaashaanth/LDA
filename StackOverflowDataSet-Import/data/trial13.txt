I want to simulate a competitive enzymatic model in Matlab: 

S + E <-> SE -> E + P
E + I <-> EI

Where S = substrate, E = enzyme, P = product, ES = enzyme-substrate-complex, I = inhibitor
m1 = sbiomodel ('simpleModel');
r1 = addreaction (m1, 'S + E -> E + P');

m1.species(1).InitialAmount = 10;
m1.Species(2).InitialAmount =1;
m1.Species(3).InitialAmount =0;

kineticLaw = addkineticlaw(r1,'Competitive-Inhibition');  

p1 = addparameter(kineticLaw,'Km',20);
p2 = addparameter(kineticLaw,'Ki',6);
p5 = addparameter(kineticLaw,'Vm',2);

sd = sbiosimulate(m1);

m1.species

sbioplot(sd);

None of this is working. I provided all the parameters needed. When I simulate it an error comes up:
Error using SBCompiler.SimulationObject/simulate
--> Error reported from KineticLaw Validation:
A parameter variable name on kinetic law '' is empty. The number of parameters on the kinetic law must match the number in its definition, and all parameter names must be set.

--> Error reported from Expression Validation:
Invalid reaction rate '' for reaction 'S + E -> E + P'. Reaction rates must be valid MATLAB expressions and cannot end in semicolons, commas, comments ('%' and optional text), or line continuations ('...' and optional text).


Error in sbiosimulate (line 140)
[t, x] = simobj.simulate(mobj, cs, variants, doses);

Error in EnzymeKineticsMMwCompInhib1 (line 38)
sd = sbiosimulate(m1);

Any ideas/hints?
