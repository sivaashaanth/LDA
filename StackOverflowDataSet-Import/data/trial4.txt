Opencart 2.x/3.x
Hi, friends!
My head is boiling like a teapot :( Help please to solve the problem.
I want to show all values from "Stock Status" in category page but do not show "Out Of Stock" when the quantity of the product more and less zero.
My code:
product.php - in controller
Before: if ($product_info['quantity'] <= 0) {
Add: $data['stock_quantity'] = $product_info['quantity'];
     $data['stock_text'] = $product_info['stock_status'];

After: $data['products'][] = array(
Add: 'quantity'       => $result['quantity'],
     'stock_text'     => $result['stock_status'],

category.php - in controller
After: $data['products'][] = array(
Add: 'quantity'       => $result['quantity'],
     'stock_text'     => $result['stock_status'],

product.tpl - in template
<?php if ($stock_status_id != 5) {
echo $product['stock_text']; }
?>

Tell please, which correct code do I need to specify so that i can see all statuses except for "out of stock"(id=5) ?
Preview
