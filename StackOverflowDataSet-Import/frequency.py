import plotly
import plotly.graph_objs



from collections import Counter

with open("/Users/sivaashaanth/Downloads/SAN-TopicModeling/extdata/question-tags/trial.txt") as f:
    wordcount = Counter(f.read().split())
#print(wordcount.keys())
x1=[]
y1=[]
for letter, count in wordcount.most_common(10):
    x1.append(letter)
    y1.append(count)
#print(x1)
plotly.offline.plot({
"data": [
    plotly.graph_objs.Bar(x=x1,y=y1)
]
})
