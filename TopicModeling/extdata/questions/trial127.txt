I am just getting started with Python3 and asynchronous programming with aiohttp. The problem I am running into is that I can make a post using the request lib and authenticate to the slack api, but when I try to use aiohttp, I cannot authenticate. I have tried all of the permutations of examples I can find on aiohttp, but have not had any luck
requests:
token = {"token": "abc123"}
requests.post('https://slack.com/api/auth.test', data=token)
{'ok': True,
 'team': 'foo',
 'team_id': 'T5YYYYYY',
 'url': 'https://foo-chat.slack.com/',
 'user': 'foobar',
 'user_id': 'XXXXXXX'}

aiohttp:
async def main():
    async with aiohttp.ClientSession() as session:
        token = {"token": "abc123"}
        async with session.post('http://slack.com/api/auth.test', data=token) as resp:
            print(resp.status)
            print(await resp.json())
loop = asyncio.get_event_loop()
loop.run_until_complete(main())
200
{'ok': False, 'error': 'not_authed'}

