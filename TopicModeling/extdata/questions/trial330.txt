Configuring SSH Keys from ePass2003 to access servers. 
I have a guest ubuntu 16.04 on VirtualBox, i am able to SSH server 1 from VM but while SSH to server 2 from server 1, getting below error.
debug3: authmethod_is_enabled publickey
debug1: Next authentication method: publickey
debug1: Offering RSA public key: /usr/lib/x86_64-linux-gnu/opensc-pkcs11.so
debug3: send_pubkey_test
debug3: send packet: type 50
debug2: we sent a publickey packet, wait for reply
debug3: receive packet: type 60
debug1: Server accepts key: pkalg rsa-sha2-512 blen 279
debug2: input_userauth_pk_ok: fp SHA256:M0HzYuvGQ8LcKpJIGPgQDrN6Xs8jpyjH4wRQdslGeV
debug3: sign_and_send_pubkey: RSA SHA256:M0HzYuvGQ8LcKpJIGPgQDrN6Xs8jpyjH4wRQdslGeV
**sign_and_send_pubkey: signing failed: agent refused operation**

When i run ssh-add -l on server 2, i can see the below output.
$ ssh-add -l
error fetching identities for protocol 1: agent refused operation
2048 SHA256:M0HzYuvGQ8LcKpJIGPgQDrN6Xs8jpyjH4wRQdslGeV /usr/lib/x86_64-linux-gnu/opensc-pkcs11.so (RSA)

I have made AllowAgentForwarding yes in /etc/ssh/sshd_config file. But still no luck in getting SSH connection to Server2 from Server1.
If anyone can help me getting through this would be great.
Thanks in Advance !!
