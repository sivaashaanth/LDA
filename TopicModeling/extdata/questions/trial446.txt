I've been using a way to unsuscribe people from a website newsletter, and that works when a user replies to an email with the word "unsuscribe".
But I want to change that so people only click a link in the email they received, redirect to my website and succesfuly unsuscribe that user. But this way makes me afraid of people being able to play with the url, write different emails and unsuscribe random people. 
I know it sounds crazy but I'm a bit paranoic with the security stuff.
I have no code since I have no idea how to achieve this the proper way, meaning no playing with the url parameters.
Thanks!
