I have an HTML input. The input is on the web-page which's opened in Chrome on the Android.
I want an option for a user to see numbers keyboard when he starts entering the value. But at the same moment, I want him to have a possibility to enter letters.

I cannot use type="number" because it doesn't allow entering letters.
I cannot use type="text" because it opens alpha keyboard by default and a user have to switch to numeric keyboard.

So the option I'm trying to find is when the standard alpha-numeric keyboard got opened but the digits input is already selected (Like when you press ?123 on the standard keyboard).
I have tried to use type="tel" but I don't understand how to switch to letters from numbers.
I'm using Cordova, so if there's no option to do this using HTML I could use native plugins if you suggest me any of them.

I use cordova so if there's no HTML way to do things I'm ready to integrate any sort of plugin, if you could suggest me anyone.
