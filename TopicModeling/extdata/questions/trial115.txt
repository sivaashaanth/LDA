Currently, we are working on QT for application development for our hardware that runs on the AM1808 processor.
At present, we developed our app using QT creator with compiler version Qt 4.8.7. 
We are planning to upgrade it with Qt 5.7 and for that, we download the corresponding package from 
http://download.qt.io/official_releases/qt/5.7/5.7.1/single/qt-everywhere-opensource-src-5.7.1.tar.gz
We successfully download this and now need to configure according to our current arm toolchain. 
we have modified the qmake.conf as per below,
#
# qmake configuration for building with arm-linux-gnueabi-g++
#

MAKEFILE_GENERATOR      = UNIX
CONFIG                 += incremental
QMAKE_INCREMENTAL_STYLE = sublib

include(../common/linux.conf)
include(../common/gcc-base-unix.conf)
include(../common/g++-unix.conf)

// modifications to g++.conf

//QMAKE_CC                = arm-linux-gnueabi-gcc

//QMAKE_CXX               = arm-linux-gnueabi-g++

//QMAKE_LINK              = arm-linux-gnueabi-g++

//QMAKE_LINK_SHLIB        = arm-linux-gnueabi-g++

// modifications to linux.conf

//QMAKE_AR                = arm-linux-gnueabi-ar cqs

//QMAKE_OBJCOPY           = arm-linux-gnueabi-objcopy

//QMAKE_NM                = arm-linux-gnueabi-nm -P

//QMAKE_STRIP             = arm-linux-gnueabi-strip

QMAKE_CC                = arm-none-linux-gnueabi-gcc

QMAKE_CXX               = arm-none-linux-gnueabi-g++

QMAKE_LINK              = arm-none-linux-gnueabi-g++

QMAKE_LINK_SHLIB        = arm-none-linux-gnueabi-g++

QMAKE_AR                = arm-none-linux-gnueabi-ar cqs

QMAKE_OBJCOPY           = arm-none-linux-gnueabi-objcopy

QMAKE_STRIP             = arm-none-linux-gnueabi-strip

load(qt_config)

after that, we run config command 
./configure -opensource -confirm-license -prefix /usr/local/Qt5 -no-pch -xplatform linux-arm-gnueabi-g++
but we found the following error,
System architecture: 'arm'
Host architecture: 'i386'
arm-none-linux-gnueabi-g++ -c -fvisibility=hidden fvisibility.c
Symbol visibility control enabled.
cc1plus: error: unrecognized command line option '-fuse-ld=gold'
arm-none-linux-gnueabi-g++ -o libtest.so -shared -Wl,-Bsymbolic-functions -fPIC bsymbolic_functions.c
bsymbolic_functions.c:2:2: error: #error "Symbolic function binding on this architecture may be broken, disabling it (see QTBUG-36129)."
Symbolic function binding disabled.
checking for C++14... 
arm-none-linux-gnueabi-g++ -c -pipe -O2 -std=gnu++1y -Wall -W -fPIC  -I. -I../../../mkspecs/linux-arm-gnueabi-g++ -o c++14.o c++14.cpp
cc1plus: error: unrecognized command line option '-std=gnu++1y'
make: *** [c++14.o] Error 1
C++14 disabled.
checking for default C++ standard edition... 
arm-none-linux-gnueabi-g++ -pipe -O2 -Wall -W -fPIC  -I. -I../../../mkspecs/linux-arm-gnueabi-g++ -o c++default.ii -E c++default.cpp
default C++ standard edition enabled.
checking for 64-bit std::atomic... 
arm-none-linux-gnueabi-g++ -c -pipe -O2 -std=gnu++11 -Wall -W -fPIC  -I. -I../../../mkspecs/linux-arm-gnueabi-g++ -o atomic64.o atomic64.cpp
cc1plus: error: unrecognized command line option '-std=gnu++11'
make: *** [atomic64.o] Error 1
64-bit std::atomic disabled.
checking for 64-bit std::atomic in -latomic... 
arm-none-linux-gnueabi-g++ -c -pipe -O2 -std=gnu++11 -Wall -W -fPIC  -I. -I../../../mkspecs/linux-arm-gnueabi-g++ -o atomic64.o atomic64.cpp
cc1plus: error: unrecognized command line option '-std=gnu++11'
make: *** [atomic64.o] Error 1
64-bit std::atomic in -latomic disabled.
checking for std::atomic for function pointers... 
arm-none-linux-gnueabi-g++ -c -pipe -O2 -std=gnu++11 -Wall -W -fPIC  -I. -I../../../mkspecs/linux-arm-gnueabi-g++ -o atomicfptr.o atomicfptr.cpp
cc1plus: error: unrecognized command line option '-std=gnu++11'
make: *** [atomicfptr.o] Error 1
std::atomic for function pointers disabled.
ERROR: detected a std::atomic implementation that fails for function pointers.
Please apply the patch corresponding to your Standard Library vendor, found in
/opt/qt-everywhere-opensource-src-5.7.1/qtbase/config.tests/common/atomicfptr
so, please suggest us correct method to compile this qt5 version.
