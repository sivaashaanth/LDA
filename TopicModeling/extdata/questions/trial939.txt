While I try to query storm over REST API:
curl -i http://hostname:8080/api/v1/topology-workers/topologyID -X GET

I`m receiving error message as:
org.apache.storm.thrift.transport.TTransportException: Frame size(1308133) larger than max lenght (1048576).

Other REST API requests are running fine. I've tried to add below parameter
to storm.yaml file but without success:
pacemaker.thrift.message.size.max:  10485760

Could you please advise how to correctly handle it so i it will return data?
