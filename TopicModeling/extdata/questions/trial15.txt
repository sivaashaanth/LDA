My form array html mark up works perfectly,
(shortened snapshot)
 <div formArrayName="partners">
   <div class="row" *ngFor="let ctrl of this.patientRestrationForm.get('partners').controls; let i = index"[formGroupName]="i">
     <div class="col-xs-8">  
      <input type="text" class="form-control" formControlName="recordId">
     </div>
     <div class="col-xs-8">
       <input type="text" class="form-control" formControlName="patientFileId">
     </div>
 </div>

now I know that we can define columns according to in-line ngx-datatable doc. 
is it possible to bind those controls to ngx-datatable defined columns  
<div formArrayName="partners">
   <ngx-datatable #mydatatable class="material" [headerHeight]="50" [limit]="5" [columnMode]="'force'" [footerHeight]="50" [rowHeight]="'auto'"
   [rows]="this.patientRestrationForm.get('partners').value">
   <ngx-datatable-column name="recordId">
      <ng-template ngx-datatable-cell-template let-rowIndex="rowIndex" let-value="value" let-row="row">
         <span title="Double click to edit" (dblclick)="editing[rowIndex + '-name'] = true" *ngIf="!editing[rowIndex + '-name']">
         {{value}}
         </span>
         <input [formControlName]="'recordId'" autofocus (blur)="updateValue($event, 'name', rowIndex)" *ngIf="editing[rowIndex+ '-name']"
         type="text" [value]="value" />
      </ng-template>
   </ngx-datatable-column>
   <ngx-datatable-column name="patientFileId">
      <ng-template ngx-datatable-cell-template let-rowIndex="rowIndex" let-value="value" let-row="row" style="margin-top: 10px;">
         <span title="Double click to edit" (dblclick)="editing[rowIndex + '-name'] = true" *ngIf="!editing[rowIndex + '-name']">
         {{value}}
         </span>
         <input [formControlName]="'patientFileId'" autofocus (blur)="updateValue($event, 'name', rowIndex)" *ngIf="editing[rowIndex+ '-name']"
         type="text" [value]="value" />
      </ng-template>
   </ngx-datatable-column>
   </ngx-datatable>
</div>

