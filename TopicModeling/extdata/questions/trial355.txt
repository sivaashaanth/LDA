I am working on adding bootstrap dual listbox controller in to web page. 
This is the code I used 
<script src="~/js/jquery.bootstrap-duallistbox.min.js"></script>
<script src="~/js/bootbox.min.js"></script>
<script type="text/javascript">
    jQuery(function ($) {
        var dualList = $('select[name="duallistbox_laws[]"]').bootstrapDualListbox({
            infoTextFiltered: '<span class="label label-purple label-lg">Filtered</span>',
          });
        }
</script>

User can selected option from first box. but it is auto sorting in the second list box. I want disable auto sorting in second listbox
Ex.
Selected order is 

But in the duallist box it showing with sorted values 
How can we disable auto sorting in second listbox?
