I can't find any documentation for the webpack 3.
Currently I'm using webpack 4 and I wanted to use the extract-text-webpack-plugin which is not supported in webpack 4, so I wanted to downgrade to webpack 3 since the extract-text-webpack-plugin is supported up to that version.
Is there any way I can get the documentation of webpack v3?
