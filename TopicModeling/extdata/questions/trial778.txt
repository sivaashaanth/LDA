I am trying to compare 2 word documents, it is not working for some word documents and working fine for others.
Below is the complete code we are using.
com.aspose.words.Document wordDoc1 =  new com.aspose.words.Document(oldDocStream);
com.aspose.words.Document wordDoc2 =  new com.aspose.words.Document(newDocStream);
wordDoc1.acceptAllRevisions();
wordDoc2.acceptAllRevisions();

if(!wordDoc1.hasRevisions() && !wordDoc2.hasRevisions()){ 
    wordDoc1.compare(wordDoc2, "Aspose Word API", new Date());
}else{
    logger.info("Aspose Limitation: Unable to compare docs, it contains unaccepted revisions");
}

Below code block is executed successfully without any exception, So I assume accepting revision is success.
wordDoc1.acceptAllRevisions();
wordDoc2.acceptAllRevisions();

But, wordDoc1.hasRevisions() returns true which means accepting revision is failed. 
As a result below message is printed in console.
"Aspose Limitation: Unable to compare docs, it contains unaccepted revisions"

Could anyone can help on this? I raised the same in official forum as well.
