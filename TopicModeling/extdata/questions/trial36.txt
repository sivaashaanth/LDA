I'm trying to set up HTTPS on my MAMP localhost server. I've followed a number of tutorials telling me to change things in httpd.conf and httpd-ssl.conf, but to no avail. I had a look through MAMP and found the SSL tab, which looks like it should be able to automatically generate a certificate and set it all up for me.
As you can see in the picture below, the SSL checkbox is greyed out, and the buttons in the SSL tab are greyed out too and don't do anything.

Is there any way I can un-grey-out the SSL tab, or if not (well, there must be,) is there anything else I could try to get HTTPS working?
