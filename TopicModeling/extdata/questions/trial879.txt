In Asp.Net MCV, you can easily return a partial view by doing the following:
return PartialView("ModelName", Model);
How is this done on a RazorPage ViewModel Handler?
