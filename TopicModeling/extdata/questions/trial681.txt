I'm trying to create a dex of the huge jar of the kotlin commandline compiler, so I can develop (i.e. compile) kotlin on a phone (in termux).
But dx runs out of RAM and is "Killed".
So, maybe divide into smaller dexs, and run as multidex? (also, close all other apps!)
Divide into dexs: fully extract the jar to files; separate with parallel directory structures; then apply dx to create one dex per root.
This worked, and I found dx could handle up to 13MB of classes, and I got the total 100MB of classes into 9 dexs.
Specify multiple dexs to dalvikvm - just like multiple jars with regular java. e.g.:
dalvikvm -cp sepdex_Main.dex:sepdex_Sep.dex sepdex.Main

NB: not /system/bin/dalvikvm, but termux's version, which fixes up the ld library path.
The next problem is that kotlinc uses dynamicInvoke, which requires sdk-level 26 (Android 8). I have Android 5.1, so the story ends here (for me).
