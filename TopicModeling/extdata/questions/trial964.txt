I have been staring at this for hours and can't find a solution and that is even though by all suggestions it SHOULD be quite easy - https://docs.microsoft.com/en-us/bot-framework/nodejs/bot-builder-nodejs-proactive-messages.
I have created a simple code which will "register" the user and save their data in my cosmosDatabse on Azure. That works perfectly.
//ON "register" SAVE USER DATA AND SAY REGISTERED MESSAGE 
bot.dialog('adhocDialog', function(session, args) {

    var savedAddress = session.message.address;
    session.userData.savedAddress = savedAddress;
    //REGISTERED MESSAGE
    session.endDialog("*Congratulations! You are now registered in our network! (goldmedal)*");
})
.triggerAction({
    matches: /^register$/i
})


But how can I then access that specific user and send him a message if, say, a condition is met? (in fact on HTTP request)

I am fairly certain we have to write the conversation ID or user ID somewhere. The question is where?
function startProactiveDialog(address) {
    bot.beginDialog(address, "A notification!");
}


This is how simple I think it should be. But where do you specify the user then?

