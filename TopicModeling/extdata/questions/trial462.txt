I have a Class A which is extended by multiple classes, say Class B, Class C and Class D. 
However I want only Class D to ignore super class fields during serialization.
How do I implement this? If I use @JsonIgnore annotation on parent Class A, all child classes get impacted.
