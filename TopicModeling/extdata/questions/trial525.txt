I got an interesting question about the VIPER architecture and Generics protocol.
It seems that the Presenter and Interactor communicates a lot, (for fetching and receiving data).
But the "Output" functions of the Interactor will always be the same
Something like
func dataFetched(object: JSONObject)

And if you have 10 modules you'll have 10 times this function with only the JSONObject that change...
Is that possible to use something like
protocol Ouput {
  associatedType Object
  func dataFetched(object: Object)
}

And choose the type of Object in the presenter ?
I hope it's clear enough.
Thanks for your help.
