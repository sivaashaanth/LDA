In spring-cloud-config, it is possible to configure a properties file and then fetch an element in the properties file using the key.
But in aws parameter store, each key value is stored as a separate entry. As i understand, i need to take each key-value from the properties file and then configure in parameter store.
In reality, each region (DEV, QA etc.) has a set of configuration files. Each configuration file has a set of properties in it. These files can be grouped based on the hierarchy support that parameter store provides.
SDLC REGION >> FILE >> KEY-VALUE ENTRIES
SDLC region can be supported by hierarchy. Key-value entries are supported by parameter store. How do we manage the FILE entity in parameter store?
