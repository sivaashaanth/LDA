I have a question concerning the ordering and alignment of data in recursively defined arrays. Consider the following code
template<int rank, int dim, class T> struct recursive_array;

template<int dim,class Tp>
struct recursive_array <0,dim,Tp> {

    using value_type = Tp;

    recursive_array<0,dim,Tp>& operator=(Tp const& val)
    {
        value = val;
        return *this;
    }
    operator Tp &() {return value;}

    value_type value;
};


template<int rank, int dim, class Tp>
struct recursive_array {

    using value_type = recursive_array<rank-1, dim, Tp>;

    recursive_array<0,dim,Tp>& operator=(Tp const& val)
    {
        for(int i = 0; i < dim; ++i)
            values[i] = val;

        return *this;
    }

    value_type& operator[] (unsigned int const i)
    {
        return values[i];
    }

    value_type values[dim];
};

The only member of recursive_array is a plain old c-array of recursive_arrays of rank-1. At the end of the recursion, i.e. rank == 0, the only member value contains the actual data. This technique is used in the finite element library deal.II to describe a class of generic Tensors, just in case you're wondering why I'm not using an ordinary c-style array.
Now let's define an array of integers 2 x 2 x 2 x 2 using the recursive_array class and a plain old c-style array and compare their data layout in the memory
//main.cc
#include<iostream>

int main () {

recursive_array<4,2,int> arr;
int carr[2][2][2][2];

for(int i = 0; i < 2; ++i)
    for(int j = 0; j < 2; ++j)
        for(int k = 0; k < 2; ++k)
            for(int l = 0; l < 2; ++l){

            int idx = i*8 + j*4 + k*2 + l;

            arr[i][j][k][l] = idx;
            carr[i][j][k][l] = idx;

            std::cout << ((int*)&(arr[0][0][0][0]))[idx]  << "   "
                      << ((int*)&(carr[0][0][0][0]))[idx] << std::endl;
        }

    return 0;
}

which creates the following output
0   0
1   1
2   2
3   3
4   4
5   5
6   6
7   7
8   8
9   9
10   10
11   11
12   12
13   13
14   14
15   15

The data appears to be stored continuously when using recursive_array as it is the case for c-style arrays. This is in accordance with the deal.II documentation, but it also says that 

the order in which the entries are presented [...] is undefined.

Or am I missing something? 
However, independent of what this documentation says, I am interested in understanding how the above code works in general. 

Why exactly is the data of the above code stored in a continuous chunk of memory? 
Is this guaranteed?
When the data is continuously stored, what's the ordering of the elements?

Thank you very much in advance!
I compiled the above code using gcc 6.3 with -std=c++11 -O0 and -std=c++11 -O3 flags, both times I got the same result.
