I try to get uploaded images in my products edit page, if i go by url i can see info of images has been uploaded but in my blade i cannot get them.
Blade
<div> // upload form for adding more images
  {{ Form::open(array('route' => 'dropzone.store', 'method' => 'post', 'files' => true, 'class' => 'dropzone', 'id' => 'my-awesome-dropzone')) }}
    <div class="fallback">
      <input name="file" type="file" multiple />
    </div>
    <input name="imageable_id" type="hidden" value="{{$product->id}}" />

    <div class="clearfix"></div>
  {{Form::close()}}
</div>
<div id="photoCounter"></div>
<div id="preview-template"></div> // previews must show in here

jQuery
<script src="{{asset('js/dropzone.min.js')}}"></script>

<script>
  var photo_counter = 0;
Dropzone.options.myDropzone = {

    acceptedFiles: ".jpeg,.jpg,.png,.gif",
    parallelUploads: 100,
    maxFiles: 15,
    maxFilesize: 8,
    previewsContainer: "#preview-template",
    addRemoveLinks: true,
    dictRemoveFile: 'Remove',
    dictFileTooBig: 'Image is bigger than 8MB',

    // The setting up of the dropzone
    init:function() {

        // Add server images
        var myDropzone = this;

        $.get('{{url('server-images')}}', function(data) {

            $.each(data.images, function (key, value) {



                var file = {name: value.name, size: value.size};
                myDropzone.options.addedfile.call(file);
                myDropzone.options.thumbnail.call(file, 'images/' + value.name);
                myDropzone.emit("complete", file);
                photo_counter++;
                $("#photoCounter").text( "(" + photo_counter + ")");
            });
        });

        this.on("removedfile", function(file) {

            $.ajax({
                type: 'POST',
                url: 'upload/delete',
                data: {id: file.name, _token: $('#csrf-token').val()},
                dataType: 'html',
                success: function(data){
                    var rep = JSON.parse(data);
                    if(rep.code == 200)
                    {
                        photo_counter--;
                        $("#photoCounter").text( "(" + photo_counter + ")");
                    }

                }
            });

        } );
    },
    error: function(file, response) {
        if($.type(response) === "string")
            var message = response; //dropzone sends it's own error messages in string
        else
            var message = response.message;
        file.previewElement.classList.add("dz-error");
        _ref = file.previewElement.querySelectorAll("[data-dz-errormessage]");
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
            node = _ref[_i];
            _results.push(node.textContent = message);
        }
        return _results;
    },
    success: function(file,done) {
        photo_counter++;
        $("#photoCounter").text( "(" + photo_counter + ")");
    }
}
</script>

Function
public function getServerImages()
  {
      $images = Image::get(['name']);

      $imageAnswer = [];

      foreach ($images as $image) {
          $imageAnswer[] = [
              'name' => $image->name,
              'size' => Filesize(public_path('images/' . $image->name))
          ];
      }

      return response()->json([
          'images' => $imageAnswer
      ]);
  }

route
Route::get('server-images', ['as' => 'server-images', 'uses' => 'ImageController@getServerImages']);


PS: currently i try to just get all uploaded images if this fixed
  later i will change my function query to get each product images only.

any idea why i cannot get my images on edit page?
