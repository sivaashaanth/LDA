I cannot get Chrome 64 to do my column breaks properly.
Here is an example chunk of HTML:
<tr><TD>
<div class="keeptogether">
<table class="header">
<tr><td><a href=#missing onclick="toggle('general-catalogue-composer0')">
<B>Willoughby Bertie, 4th Earl of Abingdon<font size=-2> (1740 - 1799)
</font></B></a></TD></TR>
<TR><td>
<div id = "general-catalogue-composer0"  style="display: none;">
<table class="detail">
<tr><td>
All hail to the myrtle shade. T.T.T.Kbd..&nbsp;&nbsp;
<a href=http://www.notamos.co.uk/detail.php?scoreid=145425 
target=_blank>Play/print/buy</a>
</td></tr>
<tr><td>
Where shall a hapless lover find. T.T.T.Kbd..&nbsp;&nbsp;
<a href=http://www.notamos.co.uk/detail.php?scoreid=145427
target=_blank>Play/print/buy</a>
</td></tr>
</table>
</div>
</td></tr>
</table>
</div>
</td></tr>

The four columns contain many of these, each with one header row and varying numbers of detail rows.
The onclick function toggles display of the detail rows for the relevant header (i.e., for the specific detail rows shown above, the div with id general-catalogue-composer0).
The div with class keeptogether should contain no column breaks.
Here is the relevant CSS stuff (the list above gets its columns via class menuindex):
.detail {
    font-size: 8px;
}
.header {
    font-size: 10px;
}
.menuindex, .menuchoral-sacred, .menuchoral-profane, .menuinstrumental {
    -moz-column-count: 4;
    -webkit-column-count: 4;
    column-count: 4;
}
.keeptogether {
    -webkit-column-break-inside: avoid;
    page-break-inside: avoid;
    break-inside: avoid;
}

It all works perfectly on the latest versions of Edge and IE - when I click on a header the detail lines appear and the columns re-jig themselves so that there are no detail lines at the tops of columns. On Chrome this simply does not happen - the display toggle is fine but Chrome appears to add column breaks wherever it likes.
My understanding is that Chrome 64 is supposed to support the CSS setup I have used.
Any ideas gratefully received!
If useful, the implementation is here:
https://www.iperimeter.co.uk/notAmosDev/VerticalTabs/vertical_tabs.php
(Click on the General Catalogue tab (the above example comes at the top - I have included only the first 2 detail rows) or on any of the next three tabs.)
