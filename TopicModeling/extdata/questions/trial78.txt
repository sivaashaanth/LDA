Let's assume I have one table in postgres with just 2 columns:

ID which is PK for the table (bigint)
time which is type of timestamp

Is there any way how to get IDs grouped by time BY YEAR- when the time is date 18 February 2005 it would fit in 2005 group (so result would be)
year  number of rows
1998  2
2005  5

AND if the number of result rows is smaller than some number (for example 3) SQL will return the result by month 
Something like
month            number of rows
(February 2018)  5
(March 2018)     2

Is that possible some nice way in postgres SQL?
