There have been questions asked similar to this but I've tried every solution I've come across in 3 straight days of chasing this issue and I can't figure it out.
This started with me not being able to import .xlsx files in the import wizard; I got the error noted here, "The 'Microsoft.ACE.OLEDB.12.0' provider is not registered on the local machine."  I tried uninstalling all Microsoft Development, Microsoft SQL, Microsoft Server, and MS Office products from my machine then re-installing only those that I use (I was initially getting some errors because I had both 32 and 64 bit applications on my machine).  
When I continued getting the Microsoft.ACE.OLEDB.12.0 error, I tried installing every Access Database Engine I could find including:

Access Database Engine 2007 
Access Database Engine 2010 Redistribute
2007 Office System Driver: Data Connectivity Components
Access 2013 Runtime
Access 2016 Runtime

All of them still produced the same error except the oldest ones.  However, all the ones that work then lead to this type of behavior.  When I get to the point where I can upload a spreadsheet, it starts normal:

But as soon as I click the Next button, it doesn't render correctly.  This only happens when doing an Excel file.

Any assistance or guidance would be greatly appreciated.
Running:
SQL Server Management Studio 17.5
Access Database Engine 2010 Redistribute (the others still resulted in the Microsoft.ACE.OLEDB.12.0 error)
Surface Book
Windows 10 Pro, 64 bit

Version 1709
OS Build 16299.251

