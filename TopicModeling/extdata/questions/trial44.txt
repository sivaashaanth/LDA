Is there a way (3rd party libraries are OK) to locate a regex match that envelops a specific "anchor" position without the need to iterate through the matches? 
For example, we have a string with a location X. Now I want to match a specific regex, with the X being part of the captured area. 
My specific scenario is about non-break conditions for punctuation marks. E.g. we have a . character, and I want to ignore it for [0-9]+[.][0-9]+.
As the string may be quite long, I need an efficient way of doing it, without the need to check several matches until I'm at the right spot. The maximum length of the interval between the start of the match and the X is unknown. 
Of course, iterating through the matches is also possible but it's not efficient because while the number of the punctuation marks is limited, the number of non-break conditions may be quite high. 
