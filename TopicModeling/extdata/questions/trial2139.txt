I'm a little bit confused. Why from one side I become CS0103(variable does not exist) and from another side CS0136(with own words - variable already exists) and declaration of variables with same names in switch works?
This works:  
var obj = new object();
switch (obj)
{
    case string str:
        break;

    case object str:
        break;
}

Here I become compilation error CS0103 "The name ' ' does not exist in the current context":  
var obj = new object();
switch (obj)
{
    case string str: 
        break;
}
if (str == null) { } //CS0103

Here I become compilation error CS0136:  
var obj = new object();
switch (obj)
{
    case string str: //<--- CS0136
        break;
}
string str = "";

CS0136: A local variable named 'var' cannot be declared in this scope because it would give a different meaning to 'var', which is already used in a 'parent or current/child' scope to denote something else 
CS0103: The name 'identifier' does not exist in the current context 
