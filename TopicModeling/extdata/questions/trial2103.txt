https://developer.nvidia.com/sites/default/files/akamai/tools/files/PerfKit_4_5_User_Guide.pdf

NVIDIA PerfKit SDK allows graphics developers access to low-level NVIDIA GPU
performance counters and NVIDIA driver counters. 

I want to understand the meaning of these counters? Are they some kind of hardware or software? What do they do?
How they are helpful to me? Please give examples of making use of them.
I have to use Nvidia perfkit to determine the performance of certain softwares dealing with robotics.
