I followed the instructions here, the summary of my parameter group changes are shown here:

I rebooted the instance and everything, and I got this new log file:

Inside error/postgres.log, I only have this:
2018-03-13 12:39:38 UTC::@:[28236]:WARNING: unrecognized configuration parameter "rds.logical_replication"
2018-03-13 12:39:38 UTC::@:[28236]:LOG: redirecting log output to logging collector process
2018-03-13 12:39:38 UTC::@:[28236]:HINT: Future log output will appear in directory "/rdsdbdata/log/error".
2018-03-13 13:07:39 UTC::@:[11128]:WARNING: unrecognized configuration parameter "rds.logical_replication"
2018-03-13 13:07:39 UTC::@:[11128]:LOG: redirecting log output to logging collector process
2018-03-13 13:07:39 UTC::@:[11128]:HINT: Future log output will appear in directory "/rdsdbdata/log/error".
2018-03-13 14:13:53 UTC::@:[14981]:WARNING: unrecognized configuration parameter "rds.logical_replication"
2018-03-13 14:13:53 UTC::@:[14981]:LOG: redirecting log output to logging collector process
2018-03-13 14:13:53 UTC::@:[14981]:HINT: Future log output will appear in directory "/rdsdbdata/log/error".
----------------------- END OF LOG ----------------------

One concerning thing is that I never received this message as the documentation said I should after applying new changes:
2013-11-05 16:48:56 UTC::@:[2952]:LOG:  received SIGHUP, reloading configuration files
2013-11-05 16:48:56 UTC::@:[2952]:LOG:  parameter "log_min_duration_statement" changed to "1" 

Any ideas what I'm doing wrong?
Update
In response to this comment:
forge=> select name,setting,source from pg_settings where name like 'log_%';
            name             |          setting           |       source
-----------------------------+----------------------------+--------------------
 log_autovacuum_min_duration | -1                         | default
 log_checkpoints             | on                         | configuration file
 log_connections             | off                        | default
 log_destination             | stderr                     | configuration file
 log_directory               | /rdsdbdata/log/error       | configuration file
 log_disconnections          | off                        | default
 log_duration                | off                        | default
 log_error_verbosity         | default                    | default
 log_executor_stats          | off                        | default
 log_file_mode               | 0644                       | configuration file
 log_filename                | postgresql.log.%Y-%m-%d-%H | configuration file
 log_hostname                | on                         | configuration file
 log_line_prefix             | %t:%r:%u@%d:[%p]:          | configuration file
 log_lock_waits              | off                        | default
 log_min_duration_statement  | 1                          | configuration file
 log_min_error_statement     | error                      | default
 log_min_messages            | warning                    | default
 log_parser_stats            | off                        | default
 log_planner_stats           | off                        | default
 log_replication_commands    | off                        | default
 log_rotation_age            | 60                         | configuration file
 log_rotation_size           | 10240                      | default
 log_statement               | all                        | configuration file
 log_statement_stats         | off                        | default
 log_temp_files              | -1                         | default
 log_timezone                | UTC                        | configuration file
 log_truncate_on_rotation    | off                        | configuration file
 logging_collector           | on                         | configuration file

Update
Same as the correct answer below, I got a response from RDS support and got this:

I would like to let you know that I can see the Log files getting generated for your instance after applying the parameter group changes. Moreover I can also see the size of these log files getting larger. The log files that are getting generated after applying the modification for parameter group and reboot(attached parameter group at 2018-03-13 12:35:58 UTC and reboot at 2018-03-13 12:39:33 UTC), are as follows:
File Name                                                   Date and Time of generation    Size
--------------------------------------------------------------------------------------------
error/postgresql.log.2018-03-13-12      2018-03-13T12:59:56+00:00     592550
error/postgresql.log.2018-03-13-13      2018-03-13T13:59:53+00:00     2258196
error/postgresql.log.2018-03-13-14      2018-03-13T15:00:00+00:00     2761617
error/postgresql.log.2018-03-13-15      2018-03-13T15:59:03+00:00     1000738
error/postgresql.log.2018-03-13-16      2018-03-13T16:59:02+00:00     582653
error/postgresql.log.2018-03-13-17      2018-03-13T17:59:01+00:00     111647
error/postgresql.log.2018-03-13-18      2018-03-13T18:59:02+00:00     66102
error/postgresql.log.2018-03-13-19      2018-03-13T19:59:02+00:00     45318


