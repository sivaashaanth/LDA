I was using instagram ?__a=1 url to read all the post of instagram's users.
A few hours ago there was a change in the response and now doesn't allow me to use max_id to paginate.
Before I usually sent a request to
https://www.instagram.com/{{username}}/?__a=1
and using the graphql.edge_owner_to_timeline_media.page_info.end_cursor in the response I called the same page with a new max_id
https://www.instagram.com/{{username}}/?__a=1&max_id={{end_cursor}}
Now the end_cursor changes in each call & max_id is not working.
Please help :)
