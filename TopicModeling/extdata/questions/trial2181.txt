I got stuck trying to build Qt libraries statically with this command:
/strg/Qt/qt-everywhere-src-5.10.0 $ ./configure -static -prefix /strg/Qt

and getting this response:
+ cd qtbase
+ /strg/Qt/qt-everywhere-src-5.10.0/qtbase/configure -top-level -static -prefix /strg/Qt
./configure: 49: exec: /strg/Qt/qt-everywhere-src-5.10.0/qtbase/configure: not found

I attempted to resolve this using vim command set userformat=unix on the file qtbase/configure.
After running the same line /strg/Qt/qt-everywhere-src-5.10.0 $ ./configure -static -prefix /strg/Qt things are no better:
Creating qmake...
'
make: *** [main.o] Error 1
.

I am following the instructions on http://doc.qt.io/qt-5/linux-deployment.html and I guess, I could make a mistake during the previous steps. Could you help me?
