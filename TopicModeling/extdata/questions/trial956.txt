Problem
I recently encountered the following message when I pushed to a GitHub repo:
"fatal: HttpRequestException encountered. "
followed by being prompted for my username and password again. 
From previous searches, Visual Studio and various others have the same root problem. For those lazier than I am, a summary of the various solutions for different applications/OS are here with relevant discussions. Hopefully people find this helpful.
Reason
Can't really call updating sercurity a problem, so here's the reason. The issue is GitHub has removed TLS-1.0 support which is causing the problem with clients including Microsoft Visual Studio's built in git client, versions of Git below 1.14 as well as GUI clients includint tortoiseGit, etc. The full release notes can be found here: 
https://githubengineering.com/crypto-removal-notice/
Solution
Edit: moved my self-answer to an answer box. See below.   
