I am trying to import 2 different rows as a list from 2 different CSV files. 
From one CSV file fetching shareNumberListand multiplierList from another CSV file. 
Then trying to set the values of shareNumberList and multiplierList in another list inputCalculationList using foreach loop. 
While trying to fetch value inside setter block(#1 & #2), then getting proper value. But while trying to fetch value from another code block(#3) Then getting zero.Though I have added setter values in a list which is declared globally. 
inputCalculationList.add(inputCalculation)
Then why I am not getting values from another block?
My full code:
public class CSVFileRWFileHandler {

    public static List<InputCalculation> inputCalculationList = new ArrayList<InputCalculation>();
    public static List<ShareNumber> shareNumberList = new ArrayList<ShareNumber>();
    public static List<Multiplier> multiplierList = new ArrayList<Multiplier>();
    public static InputCalculation inputCalculation = new InputCalculation();

public static void readCsvFile(String fileName, 
    List<InterMediateCalculation> imCalculationList) {

    ReadFilesForCalcImpl rfc = new ReadFilesForCalcImpl();
    //getting *shareNumberList*
    rfc.readShareNumberSheet(Constant.readShareNunmerFileLocation, shareNumberList); 
    //getting *multiplierList*
    rfc.readMultiplierSheet(Constant.readMultiplierFileLocation, multiplierList);
 }
 public static void writeCsvFile(String fileName) {     
    FileWriter fileWriter = null;               
    try {
        fileWriter = new FileWriter(fileName);
        fileWriter.append(Constant.FILE_HEADER);            
        fileWriter.append(Constant.NEW_LINE_SEPARATOR);            

       //block #1:
      for (Multiplier multiplier : multiplierList) {
           // setter for inputCalculation
           inputCalculation.setMultiplierAW(multiplier.getMultiplierAW());
           // getter for inputCalculation
           System.out.println(inputCalculation.getMultiplierAW());//This line prints proper value
           inputCalculationList.add(inputCalculation);              
       }
       //block #2:
       for (ShareNumber shareNumber : shareNumberList) {    
            // setter for inputCalculation                  
          inputCalculation.setShareNumberAW(shareNumber.getShareNumberAW());    
          // getter for inputCalculation
          System.out.println(inputCalculation.getShareNumberAW());//This line prints proper value
          inputCalculationList.add(inputCalculation);               
       }
      //block #3:       
      for (InputCalculation inputCalculation : inputCalculationList) {
          System.out.println(inputCalculation.getShareNumberAW());//This line don't print proper value. Getting '0' 
          System.out.println(inputCalculation.getMultiplierAW());   //This line don't print proper value. Getting '0'

          fileWriter.append(inputCalculation.getShareNumberAW()); //getter not fetchging original value.
          fileWriter.append(Constant.COMMA_DELIMITER);
          fileWriter.append(inputCalculation.getMultiplierAW());  //getter not fetchging original value.
          fileWriter.append(Constant.NEW_LINE_SEPARATOR);

       }catch (Exception e) {
        System.out.println("Error in CsvFileWriter !!!");
        e.printStackTrace();
       } 
  }

readMultiplierSheet method:
 public void readMultiplierSheet(String readfileName, List<Multiplier> multiplierList) {
    BufferedReader fileReader = null;       
    try {
        String line = "";
        fileReader = new BufferedReader(new FileReader(readfileName));
        fileReader.readLine();
        while ((line = fileReader.readLine()) != null) {
            String[] tokens = line.split(Constant.COMMA_DELIMITER);   
            if (tokens.length > 0) {
                Multiplier multiplier = new Multiplier(tokens[Constant.Cell_INDEX_OF_AW], tokens[Constant.Cell_INDEX_OF_AX]);
                multiplierList.add(multiplier);
            }
        }
    }
    catch (Exception e) {
        System.out.println("Error in CsvFileReader !!!");
        e.printStackTrace();
    } finally {
        try {
            fileReader.close();
        } catch (IOException e) {
            System.out.println("Error while closing fileReader !!!");
            e.printStackTrace();
        }
    }   
}

readShareNumberSheet method is similar to readMultiplierSheet method.
Thanks in advance.
