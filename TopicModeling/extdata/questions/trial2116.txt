So I am using omniauth and devise for LinkedIn authentication in my Rails 5 app.  
omniauth-linkedin-oauth2 (0.2.5)
devise (4.4.1)

Everything is working great for the most part and users are able to signup and authenticate using LinkedIn.  However the default image that is returned in "auth.info.image" is 50x50 which is too small.  I've done a lot of reading on this and the suggested way forward is adding "picture-urls::(original)" to the devise fields list.  This is consistent with what I see in the documentation (https://developer.linkedin.com/docs/fields/basic-profile).  This however does not seem to work and I'm wondering if there is a bug or if this is somehow outdated information.
My devise.rb omniauth config looks like:
config.omniauth Rails.application.config.omniauth, app_key, app_secret],
scope: 'r_basicprofile r_emailaddress',
fields: %w(id email-address first-name last-name picture-url picture-urls::(original))

Even after specifying these fields however, no values are returned in picture urls.
 self.remote_photo_url = auth.extra.raw_info.pictureUrls['values'].first

I've inspected the data that is returned and it's no where to be found.  Not sure what I could be missing but any advice would be greatly appreciated!
