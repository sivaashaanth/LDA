I've created a table using bash looping over files with a praat script. (A software for extracting phonetic features). 
In Praat I used the command appendInfoLine in contrast to just appendInfo which should do its work by creating new lines every time. 
I saved the result in a .txt file. It looks good when opened in a text editor. However, when I open it in a simple .txt notepad on Windows there are no new lines being edited and the second line starts just after the first one ends. 
How can it be fixed?
