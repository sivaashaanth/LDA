I'm converting a project to Swift 3 and I'm getting this error below:
 fileprivate class func moveAi() -> DataModel {
        let player = pathForPlayer(true)

        let i: Int

        let scope = GameModel.shared.scopeForPlayer(GameModel.shared.topPlayer.id, rival: GameModel.shared.downPlayer.id)


        //for (i = player.count-1; i > 0; i -= 1) { // Original for loop prior to Swift 3
        for i in stride(from: player.count-1, to: 0, by: -1) {

            if scope.contains(player[i]) {
                break
            }
        }

        // Error is on this line
        return DataModel.idConvertToPlayer(player[i], player: true)
    }

// Path for Player
class func pathForPlayer(_ play: Bool) -> [Int] {
        let player = play ? GameModel.shared.topPlayer.id : GameModel.shared.downPlayer.id
        let end = play ? topEnd : downEnd
        return pathForPlayer(Node(data: player, parent: -1), end: end)
    }

If I move the return within the for loop the error goes away, but I still need to return a value here.
Any suggestions?
Updated Code:
    fileprivate class func moveAi() -> DataModel {
        let player = pathForPlayer(true)

        let _: Int // i

        var final: Int?

        let scope = GameModel.shared.scopeForPlayer(GameModel.shared.topPlayer.id, rival: GameModel.shared.downPlayer.id)

        **//for (i = player.count-1; i > 0; i -= 1)** {
        for i in stride(from:player.count-1, to:0, by:-1) {

            final = i

            if scope.contains(player[i]) {
                break
            }
        }

        return DataModel.idConvertToPlayer(player[final!], player: true)
    }

The above code does not provide the crash I was previously getting, but I am getting a new crash.
The best way to describe is my AI will prevent itself from finishing a game.
I'm building off the board game Quoridor. The AI will build walls around it's pawn so it can't move any further. This does not happen every game though. It's very random when it happens.
Could it be based on how I've written the for loop? Is the Swift way correct or could I get insight to how to right the commented for loop in Swift syntax?
