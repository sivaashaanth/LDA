I am doing the following which perfectly works
    //else proceed with the checks
    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(
            Request.Method.GET,
            checkauthurl,
            null,
            new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(String response) { 
                         //do stuff here
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                   // do stuff here
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    HashMap<String, String> headers = new HashMap<String, String> ();
                    TokenService tokenservice = new TokenService(ctx);
                    String accesstoken = tokenservice.getToken(ApiHelper.ACCESS_TOKEN_SHARED_PREF);
                    headers.put("Authorization", "Bearer " + accesstoken);

                    return headers;
              }
    };

    // Access the RequestQueue through your singleton class.
    ApiSingleton strngle = new ApiSingleton(ctx);
    strngle.addToRequestQueue(jsonObjectRequest);

For every request, I have to add the request header.  How can I set request headers directly in the singleton.
This is my singleton
private static ApiSingleton mInstance;
private RequestQueue mRequestQueue;
public static Context mCtx;
private ImageLoader mImageLoader;

public ApiSingleton(Context context) {
    mCtx = context;
    mRequestQueue = getRequestQueue();
    //do stuff
}

public RequestQueue getRequestQueue() {
    if (mRequestQueue == null) {
        // getApplicationContext() is key, it keeps you from leaking the
        // Activity or BroadcastReceiver if someone passes one in.
        mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
    }
    return mRequestQueue;
}

How do I avoid the above code duplication when attaching the bearer token in every request?
