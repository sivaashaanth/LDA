I'm trying to do a simple OAUTH application with the Google api.
In the middle of the procedure I'm trying to fetch the access code from an opened window after the authorization since the opened window passes through multiple redirects. I was wondering how can I get the code from the final page before closing so I can continue with using it.
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>quick demo</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.7/angular.min.js"></script>
</head>
<body ng-app="app" ng-controller="ctrl">

<img src="folder/mGHPI.png" ng-click="login()">
</body>
<script>
    var app= angular.module("app",[]);
    var url;
    var openedwindow;
    var code;
    app.controller("ctrl",function($scope,$http,$window){
        $http.get("url").then(function(response){
            url=response.data
        });
        $scope.login=function(){
            openedwindow=$window.open(url,"sign in with google","width=500px,height:700px")
            code =openedwindow.document.URL;

            console.log(code)
        };
        //window.opener.postMessage("my  url is :"+location.href,"*");

        window.onmessage = function(e){
            console.log(e)
        }
    })
</script>
</html>

