I have been having this problem for quite a while, that my com.android.support:design:26.1.0 fails me and I have no clue what to change anymore.
These are my files for module gradle. 

Build.gradle (app)

apply plugin: 'com.android.application'

android {
    compileSdkVersion 26
    buildToolsVersion '26.0.2'

    defaultConfig {
        applicationId "com.xxx.yyy"
        minSdkVersion 16
        targetSdkVersion 26
        versionCode 1
        versionName "1.0"
        testInstrumentationRunner "android.support.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        release {
            minifyEnabled false
            proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
        }
    }
}

dependencies {
    implementation fileTree(dir: 'libs', include: ['*.jar'])
    implementation 'com.android.support:appcompat-v7:26.1.0'
    implementation 'com.android.support.constraint:constraint-layout:1.0.2'
    implementation 'com.android.support:design:26.1.0'
    testImplementation 'junit:junit:4.12'
    androidTestImplementation 'com.android.support.test:runner:1.0.1'
    androidTestImplementation 'com.android.support.test.espresso:espresso-core:3.0.1'
    compile 'com.android.support:support-v4:26.1.0'
    compile 'com.google.android.gms:play-services-vision:9.4.0'
//    compile 'com.android.support:design:26.1.0'
}

//apply plugin: 'com.google.gms.google-services'


The project gradle file

// Top-level build file where you can add configuration options common to all sub-projects/modules.

buildscript {

    repositories {
        google()
        jcenter({ url "http://jcenter.bintray.com/" })
    }
    dependencies {
        classpath 'com.android.tools.build:gradle:3.0.1'
//        classpath 'com.google.gms:google-services:3.2.0'

        // NOTE: Do not place your application dependencies here; they belong
        // in the individual module build.gradle files
    }
}

allprojects {
    repositories {
        google()
        jcenter({ url "http://jcenter.bintray.com/" })
        maven { url "https://maven.google.com" }
    }
}

task clean(type: Delete) {
    delete rootProject.buildDir
}


Error:Unable to resolve dependency for
  ':app@releaseUnitTest/compileClasspath': Could not resolve
  com.android.support:design:26.1.0. 
Error:Unable to resolve dependency
  for ':app@release/compileClasspath': Could not resolve
  com.android.support:design:26.1.0. 
Error:Unable to resolve dependency
  for ':app@debugUnitTest/compileClasspath': Could not resolve
  com.android.support:design:26.1.0. 
Error:Unable to resolve dependency
  for ':app@debugAndroidTest/compileClasspath': Could not resolve
  com.android.support:design:26.1.0. 
Error:Unable to resolve dependency
  for ':app@debug/compileClasspath': Could not resolve
  com.android.support:design:26.1.0.

And only the 'Snackbar' function does not work but rest of it seems fine.
When I change the design to 25.1.0 version instead, it works but it clashes with other 26.1.0 ones so it's not recommended by the Android Studio. Anyone..?
I have tried putting the 
compile 'com.android.support:design:26.1.0'

instead of implement, it does not work. When I tried to change the whole thing to either 25.4.0 or any other 25 version, a lot of other things doesnt compile as well. I have unchecked the Offline work, tried removing / adding maven directory, un-commenting a few more of dependencies, changing all to compile instead of implement but just gives me more errors.
Yes I have updated all my SDKs to the newest but I have a feeling that Gradle just cannot download certain dependencies from the websites that's why I cannot download any more of the other dependencies. (Although my off-line work is unchecked)
Also, whatever that are uncommented in the code above, it means I have tried inputting those lines but it did not solve the issue or rather created more issues.
