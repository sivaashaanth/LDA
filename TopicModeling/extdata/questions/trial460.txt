Im trying to alter the allowed HTML tags that can be used within the comment-section of my page.
Right now I don't understand why my custom function is being overwritten whenever I use the pre_comment_content filter for that particular function.
functions.php
function be_allowed_html_tags_in_comments() {

  global $allowedtags;

  $allowedtags = array(
      'ul' => array(),
  );
}
add_action('pre_comment_content', 'be_allowed_html_tags_in_comments', 9);

which outputs the following allowed HTML: 
<a href="" title=""> <abbr title=""> <acronym title=""> <b> <blockquote cite=""> <cite> <code> <del datetime=""> <em> <i> <q cite=""> <s> <strike> <strong>

My tag is not even in there.
But when I hook the function into the init filter. My and only my tag/s are shown as allowed tags.
function be_allowed_html_tags_in_comments() {

  global $allowedtags;

  $allowedtags = array(
      'ul' => array(),
  );
}
add_action('init', 'be_allowed_html_tags_in_comments', 9);

Do I oversee something? Or isn't pre_comment_content even the right filter there? Im kinda scared when I use init, it may interfere with something else later on in the project.
