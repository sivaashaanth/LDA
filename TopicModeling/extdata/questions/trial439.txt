I'm making a virtual deejay with processing. it works with blob detection which trains the camera to read color from and interact with it. So I'm having a record image as png on the interface so I want it to spin the image as a real DJ. However, when I use rotate, it changes the position of the image and it is not spinning, so how can I solve this problem? 
The code that I provided is only the part that has the Image, it is not the whole code of the application.
void setup() {

    size(1280, 720);

    String[] cameras = Capture.list();
    printArray(cameras);
    video = new Capture(this, 1280, 720);
    video.start();
    prev = createImage(video.width, video.height, RGB);
    trackColor = color(255, 0, 0);

    rec = loadImage("rec.png");  
    slide = loadImage("slide.png");  
    slider = loadImage("slider.png");
    frameRate(150);

}

void draw() {

    video.loadPixels();
    image(video, 0, 0);
    //this makes the camera flips
    image(video,0, 0, width, height); 
    translate(width, 0);
    scale(-1.0, 1.0);
    image(video,0, 0, width, height); 

    //the image That need to be spinning
    rotate(millis() * 0.0005);
    tint(255, 180);
    image(rec,100,200,465,465);
    noTint();

}

