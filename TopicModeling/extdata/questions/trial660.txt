FIRST see below EDIT part.
I use beaglebone black, ker 3.8, and GCC compiler for a signal processing project. 
I receive the raw data from three GPS modules through uart communication asynchronously. So, I used 3 threads which check 3 UARTs (BB-UART1, BB-UART2, BB-UART4) continuously to receive raw data, here called the "reading threads". after each data packet received from each module, I decode the received data packet and extract important data packet. 
when, decoding is finished in the threads, I perform the signal processing in a separate thread, called "signal processing thread", using above 3-important decoded data packet.
as it's obvious, I should synchronize the reading threads with signal processing thread. I use pthread_cond_wait and pthread_cond_signal for that.
the code operates fine and synchronization and signal processing are performed efficiently. each data packet received in 0.1 second (10 times in a second).
In the signal processing thread, after signal processing, I send the signal processing result to the user through separate UART, BB-UART5.
when I add this part of the code, "THIS" line, after some time, which all parts or OK and the signal processing results are sent to user, the signal processing thread is frozen and locked in a mutex. In fact the mutex unlocking is not performed in previous step.
I spent many time, some weeks, to find the reason. when I remove the mutex and other tools for threads synchronization (to make the whole code simple to debug) an array of data in somewhere is overflowed and its data are changed to overflowed values. However when I don't add "THIS" line, overflow not occurred any time.
when I remove the "write" function (of BB-UART5) in signal processing thread all operations are OK.
the signal processing thread:
void *signal_processing_thread (void *arg){

int i, j;
char str[512];

printf("000000000000000000000000000000000000000000\r\n");
printf("0-signal_processing_thread is running!\r\n");
printf("000000000000000000000000000000000000000000\r\n");

while(1){
    pthread_mutex_lock(&th1); // the code lock here after add "THIS" line
    pthread_mutex_lock(&th2);
    pthread_mutex_lock(&th3);

    if ((!decode_completed[0])|(!decode_completed[1])|(!decode_completed[2])){
        pthread_mutex_unlock(&th1);
        pthread_mutex_unlock(&th2);
        pthread_mutex_unlock(&th3);
        continue;
    }
    // data packets are ready in reading threads

    // signal processing start



    // signal processing done

    // send the results
    sprintf (str,"some string\r\n\0",some variables);
    printf (str);

    for (i=0;i<256;i++)
        if (str[i]==0)
             break;
    write (uart5_id, str, i); // "THIS" line

    decode_completed [0] = 0;
    decode_completed [1] = 0;
    decode_completed [2] = 0;

    pthread_cond_signal(&cv1);
    pthread_mutex_unlock(&th1);

    pthread_cond_signal(&cv2);
    pthread_mutex_unlock(&th2);         

    pthread_cond_signal(&cv3);
    pthread_mutex_unlock(&th3);
}

printf("signal processing thread is closed!\r\n");
}

the reading threads:
void *getdecodedata1_thread (void *arg){
    int ret, count, count_decode=0;
    char buffer[2024], buffer_decode[2024];
    int i;

    printf("1-getdecodedata_thread is running!\r\n");

    count = 0;

    pthread_mutex_lock(&th1);       
    while(1){
        for (i=0;i<500;i++){    
            ret = read(uart1_id, buffer+count ,255); 

            if (ret<1)
                continue;

            // data received

            count += ret;
            if (count>1000) break;              
        }
        if (count>0){ // packet received
            for (i=0;i<count;i++) 
                buffer_decode1[count_decode3+i]=buffer[i];

            count_decode1 += count;

            if (count_decode1>15){
                // decode
                // ....
                // decode done
            }
            count = 0;

        }
        if  (decode is completed) {     

            decode_completed [0] = 1; // newdata

            printf("WAIT_1\n");

            pthread_cond_wait(&cv1,&th1);
            pthread_mutex_unlock(&th1);
            pthread_mutex_lock(&th1);

            printf("RELEASE_1\n");
        }
    }
 }  
void *getdecodedata2_thread (void *arg){
    int ret, count, count_decode=0;
    char buffer[2024], buffer_decode[2024];
    int i;

    printf("2-getdecodedata_thread is running!\r\n");

    count = 0;

    pthread_mutex_lock(&th2);       
    while(1){
        for (i=0;i<500;i++){    
            ret = read(uart2_id, buffer+count ,255); 

            if (ret<1)
                continue;

            // data received

            count += ret;
            if (count>1000) break;              
        }
        if (count>0){ // packet received
            for (i=0;i<count;i++) 
                buffer_decode3[count_decode2+i]=buffer[i];

            count_decode2 += count;

            if (count_decode2>15){
                // decode
                // ....
                // decode done
            }
            count = 0;

        }
        if  (decode is completed) {     

            decode_completed [1] = 1; // newdata

            printf("WAIT_2\n");

            pthread_cond_wait(&cv2,&th2);
            pthread_mutex_unlock(&th2);
            pthread_mutex_lock(&th2);

            printf("RELEASE_2\n");
        }
    }
}

void *getdecodedata3_thread (void *arg){
    int ret, count, count_decode=0;
    char buffer[2024], buffer_decode[2024];
    int i;

    printf("3-getdecodedata_thread is running!\r\n");

    count = 0;

    pthread_mutex_lock(&th3);       
    while(1){
        for (i=0;i<500;i++){    
            ret = read(uart4_id, buffer+count ,255); 

            if (ret<1)
                continue;

            // data received

            count += ret;
            if (count>1000) break;              
        }
        if (count>0){ // packet received
            for (i=0;i<count;i++) 
                buffer_decode3[count_decode3+i]=buffer[i];

            count_decode3 += count;

            if (count_decode3>15){
                // decode
                // ....
                // decode done
            }
            count = 0;

        }
        if  (decode is completed) {     

            decode_completed [2] = 1; // newdata

            printf("WAIT_3\n");

            pthread_cond_wait(&cv3,&th3);
            pthread_mutex_unlock(&th3);
            pthread_mutex_lock(&th3);

            printf("RELEASE_3\n");
        }
    }
}

EDIT: I've found that the problem is in another place of the code. in some where I use "write" function to send some bytes to UART5 and in a separate thread I read simultaneously (non-blocking) UART5 to receive commands. I think a problem like "SegFault" is occurred and the above problem is seen. when I comment the "read" function of UART5, all things is correct and mutexes work finely. How can I use the UART5 to read and write simultaneously?
