I have inherited some CakePHP code recently and am working on containerizing it into docker. I am not that fluent in CakePHP. Everything has been successful except a page which errors out giving me the error:
Call to undefined function App\Model\Table\_()

The stack trace leads me to a cake template file that outputs a form and some inputs. There are certain inputs that throw the error. If I add 'required' => false to the options the error goes away. But I am trying not to edit the code as it works perfectly on a dev server. I have copied over the app.php also.
Code that errors:
echo $this->Form->input('password', ['value'=>'']);

Code that works:
echo $this->Form->input('password', ['value'=>'', 'required' => false]);

Dev server:
PHP - 7.0.22
CakePHP - 3.3.6
Local Docker Container:
PHP - 7.1.5
CakePHP - 3.3.6
There is another error along with that main error that suggests this:
Could this be caused by using Auto-Tables?

Please try correcting the issue for the following table aliases:

ArosAcos

Could this be an issue where I did not copy everything from the dev database into my local database? 
Stack trace:
>  App\Model\Table\UsersTable->validationDefault
CORE/src/Validation/ValidatorAwareTrait.php, line 108

>  Cake\ORM\Table->validator
CORE/src/View/Form/EntityContext.php, line 463

>  Cake\View\Form\EntityContext->_getValidator
CORE/src/View/Form/EntityContext.php, line 406

>  Cake\View\Form\EntityContext->isRequired
CORE/src/View/Helper/FormHelper.php, line 2508

>  Cake\View\Helper\FormHelper->_initInputField
CORE/src/View/Helper/FormHelper.php, line 1610

>  Cake\View\Helper\FormHelper->file
APP/Template/Users/edit_profile.ctp, line 15

Edit Profile Code:
<div class="displaysection">
        <?= $this->Form->create($user); ?>
        <?php
            echo $this->Form->input('old_password', ['type' => 'password', 'required' => true]);
            echo $this->Form->input('password', ['value'=>'']);
            echo $this->Form->input('password_confirm', ['type' => 'password', 'required' => true]);
        ?>
        <?= $this->Form->button('Change Password') ?>
        <?= $this->Form->end() ?>
      </div>

I am also getting this error when I submit the form. I guess this is an issue that might pop up in a ton of places.
I tracked it down to a file in CakePHP.
CORE/src/Validation/ValidatorAwareTrait.php, line 116

$validator = $this->{'validation' . ucfirst($name)}($validator);

Apparently this is the line that is causing the error. I have no idea why this is working on the dev and production servers but not my local docker container.
Any help on this would be appreciated. Thanks!
