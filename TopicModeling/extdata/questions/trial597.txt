I recently upgraded my angular4 project to angular 5 project. When running the app, Foundation modal can open as intended. However, when running UT using Karma, the UT always complains about:
Failed: $(...).foundation is not a function
Tried the following things already:

Included foundation in tsconfig.spec.ts
Included foundation in files in karma.conf.js
Included foundation in test.ts as well

and the closest thing is this workaround Foundation.addToJquery($); that is mentioned in Foundation forum discussion here. However, this works for all foundation() except foundation('open').
I created a simple app here for everyone to reproduce: my-app1.
You just need to run npm run test and you can reproduce the issue. I didn't have this issue in angular4.
Anyone having the same issue in angular5?
I am using Angular 5 and Foundation 6.4.3.
