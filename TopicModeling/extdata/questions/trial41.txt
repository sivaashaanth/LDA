I have a region:
Items Quantity Amount
"Item A" 423 63
"Item B and C" 27 169
"Item D " 6 199

I would like to create org-table of the above region: 
|Items          | Quantity| Amount|
----------------|------------------
|"Item A"       |  423    | 63    |
|"Item A and C" |  27     | 169   |
|"Item D"       |  6      | 199   |

The current suggestion is to select the region and use C-c | to create the table but this creates a table based on spaces between the elements. It is not able to group the items column. Is there a way I can group 
