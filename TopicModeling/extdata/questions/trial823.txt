I am using Angular PrimeNG and I have the following scenario
<button (click)="display = !display">Open</button>
<p-dialog [(visible)]="display">
    <custom-component [uid]="userId"></custom-component>
</p-dialog> 

Problem here is whenever I try to open the p-dialog, the custom component is not called again, how do I make sure the component is loaded again to make sure the modal has latest data every time its opened?
