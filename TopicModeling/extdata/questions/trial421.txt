I have a vue.js/Phoenix app. I'm trying to understand how to properly configure the frontend assets. I'm having trouble understanding why my priv/static/js/app.js file keeps updating whenever I Change something in other files. I'm trying to research this behavior but I can't seem to find out any information. 
app.html.eex
<body>
  <%= render @view_module, @view_template, assigns %>
  <script src="<%= static_path(@conn, "/js/app.js") %>"></script>
</body>

My basic question is how to structure a vue.js app? The fact that I change something in asset/src dynamically changes something in static/js/app.js seems really strange. Does anybody have resources or answers on what might be happening here or places I can go to learn more?
