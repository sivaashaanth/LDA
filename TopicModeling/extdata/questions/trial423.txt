I'm using Jint for executing javascript in my c# project and I need to access functions like map, filter, reduce etc on an Array.
From what I've been able to find here, the native array functions are not available in Jint, but, underscore.js is included in the Jint package, but how do I access it to use _.map() etc?
As a corollary, I would like to use arrow functions for the conditions in functions like filter, but Jint currently complains about > being an unexpected character. 
I'm using the v3.0.0-Beta-1138 nuget package.
