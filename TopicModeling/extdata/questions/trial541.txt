I'm using Golang SDK in order to Put/Delete entities from Google Cloud Datastore, and I want to be sure that when the operation (Put/Delete) is finished, other clients will get the new entity (in case of "Put"), or would not get the deleted entity in case of "Delete".
Using busy-waiting in order to check if operation finished will not work, because an updated entity value may not be immediately visible to other replicas when executing a query in google datastore, see: Eventual Consistency when Reading Entity Values.
Here is a code that waits for datastore consistency using sleep, is there a better solution? 
ds := newDSClient()
if err := ds.Delete(context.Background(), datastore.NameKey("book", "1", nil)); err != nil {
    log.Infof("failed to delete book ID: 1 with '%v'", err)
} else {
    // wait for datastore persistence
    time.Sleep(time.Millisecond * 1500)   
}

Thanks in advanced,
Effi
