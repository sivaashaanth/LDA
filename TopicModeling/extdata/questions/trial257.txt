In my default collection, I have a team project. In that team project, I have 2 branches : PROD and DEV.
Lately, I've been working on DEV mainly. So since the number of changesets are common between PROD and DEV, I have the current issue :
One of the file has a changeset number in PROD (2470) is lower than the one in DEV (2539) (from what I can see in the merge tool)
When I did a merge from PROD to DEV, the first time, TFS did not see that the file needed to be merged. 
I had a lot of compilation errors. So I had to modify the file in DEV, do a check-in and then, during the merge, TFS saw it and asked me in the resolve conflicts tab to deal with the file.
BUT,
in the merge tool, all the new code in DEV are overwritten by the code in PROD. I don't understand ! 
For example, the file in DEV has some new using directives. But the merge tool removed them because it's picking the PROD file. So most of the new code is hidden by the lower changeset.
What's going on ? I am afraid to merge or using TFS now !
Did you have a similar problem ? What can I do ?
Added a picture of the source explorer tree :
source explorer tree
