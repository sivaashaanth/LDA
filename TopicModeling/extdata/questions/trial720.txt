I have an Angular project on my local machine that builds without errors using Angular AOT and successfully runs on the server. However, when you clone the project repo and run the same build scripts after running npm install, it builds without error but when you visit the site you receive:
ReferenceError: i0 is not defined ReferenceError: i0 is not defined...

I tried looking up the error and it seems to have something to do with socket.io. I am not using socket.io directly in my project. I figured out there is a difference in node_modules causing the error because I copied and pasted the working node_modules into a fresh clone without running npm install and after building, everything worked without error.
My question is what is how do I sift through node_modules in each project to figure out the culprit of the error?
