What is the correct way to receive UDP broadcast packets in netcat or socat?
I would like to send broadcast from one computer to another in bash. Sending works well with:
socat - UDP-DATAGRAM:255.255.255.255:50011,broadcast

But (in both cases) receiving freezes when first line (packet) received:
nc -luk 50011

or
socat - UDP-LISTEN:50011

I think that the UDP connection ends, but the receiving side does not hang or does not start to listen to new packets. It is still waiting for the first connection (ignoring new connections).
