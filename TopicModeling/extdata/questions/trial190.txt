I am trying to access the Joined Groups from Microsoft Graph API,
I have done the Azure AD authentication and able to get the required access token.
The access token is working when I read the data from SharePoint OData endpoints but I am not able to access the URL
 https://graph.microsoft.com/beta/me/joinedTeams

I tried to access using PostMan client and it gives me the error
{
  "error": {
    "code": "AuthenticationError",
    "message": "Error authenticating with resource.",
    "innerError": {
      "request-id": "ef4be9c8-27c7-40e7-8157-c08848f5132f",
      "date": "2018-03-13T09:46:11"
    }
  }
}

When I tried to access this URL using javascript code then I got the different error
{
  "error": {
    "code": "BadRequest",
    "message": "Invalid version",
    "innerError": {
      "request-id": "51113cc5-2963-4e0f-bf70-6e080a8f5671",
      "date": "2018-03-13T09:29:18"
    }
  }
}

I tried with different permission set, even with administrative access but no luck.
I also tried to access
https://graph.microsoft.com/beta/me/

it is working in the PostMan Client but, not working in the JavaScript (I got the same error).
Tere is the header I use to send Request
var header = {
  Authorization: "Bearer <ACCESS_TOKEN>",
  Accept: "application/json;odata.metadata=minimal;odata.streaming=true;IEEE754Compatible=false"
};

Can Anyone suggest What things I missed or What permission I should use?
These permission require admin consent, are these the correct permissions?

Update 2
I tried to use admin consent, grant all required permission to the app(AD app). below are the scope data extracted from jwt.io
    "scp": "email Group.Read.All Group.ReadWrite.All openid User.Read User.Read.All User.ReadBasic.All User.ReadWrite.All"
    "aud": "https://graph.microsoft.com/"

I am using administrator account with all permissions. 
but no luck, still I am getting "Error authenticating with resource."
but using the token I can access the URL
  https://graph.microsoft.com/v1.0/me

and in response, I am getting all the details of Admin
I tried to read all the documentation available for that but no luck
