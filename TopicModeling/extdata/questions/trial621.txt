I have the following code:
name=input("What do you want to call your file?")
print(name)
name=name+".txt"
print(name)

When I run it, I get:
<whatever was input>
.txt

I want to get:
<whatever was input>
<whatever was input>.txt

I have tried doing:
input("What do you want to call your file?")+".txt"

And:
input("What do you want to call your file?")
+=".txt"

but no luck.
What is curious is that when I run it in the shell, it works l, and also that it works in python 3.1 but not 3.4.
