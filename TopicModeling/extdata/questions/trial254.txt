I am trying to parse remote json object that has dynamic key values. Here is my json format.
[
    {
        "Item": [
            {
                "Account": "Flowers",
                "Today": 1,
                "Tomorrow": 1
            }
        ]
    }
]

I couldn't use the POJO class here because the remote json keys are always dynamic. So, I tried with the way as below with Retrofit2.
call.enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {

                Log.e("Here response is", response.body().toString() + " ");       // got "Here response is: [{"Item":[{"Account":"FLOWSERVE-FCD","Today":1,"Tomorrow":1}]}] "

                JsonArray responseArray= response.body().getAsJsonArray();

                try {
                    JsonObject jObj1 = (JsonObject) responseArray.get(0);
                    JsonArray jArray = jObj1.getAsJsonArray("Item");
                    JsonObject jObj2 = (JsonObject) jArray.get(0);

    // It is ok if we know the keys
    String value1 = jObj2.get("Account").toString();     // get FLOWSERVE-FCD
    String value2 = jObj2.get("Today").toString();      // get 1
    String value3 = jObj2.get("Tomorrow").toString();   // get 1

    // Here , I try for dynamic, but it got error
        ArrayList<String> dynamicKeys = new ArrayList<String>();   // create ArrayList to store dynamic keys later

                    for (int i = 0; i < jObj2.size(); i++) {
                        String str_image_url = jObj2.get(i);       //   although use "jObj2.keys(); it can't be used here.              
                        Log.e("Succcccc ", str_image_url);
                        dynamicKeys.add(str_image_url);
                    }



                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("Grrr Again", e.toString());
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Toast.makeText(context, t.getMessage(), Toast.LENGTH_LONG).show();
                Log.e("Grrr Error", t.getMessage());
            }
        });

It works well if we know the json keys, but it trapped for me with dynamic keys. So, any ideas or alternative ways are appreciating.
