I used asynchronous C++ client for gRPC,but I can't set timeout for each request,when I used synchronous client, I can set the timeout through the ClientContext.set_deadline(),but this method does not take effect in asynchronous client;
I worried about when the gRPC server crash, then some request will be in the completion queue for ever, however I hope that when the request in the completion queue did not receive a response within a specified period of time , then it can notify that the request is timeout.
how can I achieve this goal?
waiting for your answer, thank you!
