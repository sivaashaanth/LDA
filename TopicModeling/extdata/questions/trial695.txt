I am starting to learn android but havent been able to get anywhere. I downloaded android studio as per instructions and im trying to set up an AVD.
I am using a Mac Air(macOS High Sierra) with java 1.8.0_51.  
The moment i launch an AVD there is a warning in the event log: 
"Emulator: Warning: Quick Boot / Snapshots not supported on this machine. A CPU with EPT + UG features is currently needed. We will address this in a future release." 
The emulator just gets stuck on the loading page. 
So far i have tried to :
1. Reduce AVD Ram size
2. Increase AVD heap size
3. change to "software" to avoid issues if any with my graphics card
4. Install/ delete many versions of phones/api levels for avd
In the Logcat logs i see the following errors:

03-09 17:27:15.544 0-1114/? I/lowmemorykiller: No kernel
  memory.pressure_level support (errno=2) 03-09 17:27:15.545 0-1114/?
  E/lowmemorykiller: Kernel does not support memory pressure events or
  in-kernel low memory killer

.....

03-09 17:30:28.470 1122-1122/? I/iptables: iptables v1.4.20: can't
  initialize iptables table 'nat': Table does not exist (do you need to
  insmod?) 03-09 17:30:28.470 1122-1122/? I/iptables: Perhaps iptables
  or your kernel needs to be upgraded. 03-09 17:30:28.470 1122-1122/?
  I/iptables: iptables terminated by exit(3) 03-09 17:30:28.629
  1122-1122/? E/Netd: exec() res=0, status=768 for /system/bin/iptables
  -t nat -N oem_nat_pre  03-09 17:30:30.888 1122-1122/? I/iptables: iptables v1.4.20: can't initialize iptables table 'nat': Table does
  not exist (do you need to insmod?) 03-09 17:30:30.888 1122-1122/?
  I/iptables: Perhaps iptables or your kernel needs to be upgraded.
  03-09 17:30:30.902 1122-1122/? I/iptables: iptables terminated by
  exit(3) 03-09 17:30:31.112 1135-1135/? E/memtrack: Couldn't load
  memtrack module (No such file or directory) 03-09 17:30:31.114
  1135-1135/? E/android.os.Debug: failed to load memtrack module: -2
  03-09 17:30:31.257 1122-1122/? E/Netd: exec() res=0, status=768 for
  /system/bin/iptables -t nat -A PREROUTING -j oem_nat_pre  03-09
  17:30:35.605 1122-1122/? I/iptables: iptables v1.4.20: can't
  initialize iptables table `nat': Table does not exist (do you need to
  insmod?) 03-09 17:30:35.607 1122-1122/? I/iptables: Perhaps iptables
  or your kernel needs to be upgraded. 03-09 17:30:35.607 1122-1122/?
  I/iptables: iptables terminated by exit(3)

....
Please let me know what i can do to fix this and get any emulator working!
