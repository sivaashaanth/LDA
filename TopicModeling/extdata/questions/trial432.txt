I am trying to run a function every time a file changes. Watchdog seems to be the main solution to do this without polling.
My code however returns an error:
Exception in thread Thread-14:
Traceback (most recent call last):
  File "C:\Users\Administrator\Anaconda3\lib\threading.py", line 916, in _bootstrap_inner
    self.run()
  File "C:\Users\Administrator\Anaconda3\lib\site-packages\watchdog\observers\api.py", line 146, in run
    self.queue_events(self.timeout)
  File "C:\Users\Administrator\Anaconda3\lib\site-packages\watchdog\observers\read_directory_changes.py", line 77, in queue_events
    winapi_events = read_events(self._handle, self.watch.is_recursive)
  File "C:\Users\Administrator\Anaconda3\lib\site-packages\watchdog\observers\winapi.py", line 347, in read_events
    buf, nbytes = read_directory_changes(handle, recursive)
  File "C:\Users\Administrator\Anaconda3\lib\site-packages\watchdog\observers\winapi.py", line 307, in read_directory_changes
    raise e
  File "C:\Users\Administrator\Anaconda3\lib\site-packages\watchdog\observers\winapi.py", line 303, in read_directory_changes
    ctypes.byref(nbytes), None, None)
  File "C:\Users\Administrator\Anaconda3\lib\site-packages\watchdog\observers\winapi.py", line 108, in _errcheck_bool
    raise ctypes.WinError()
OSError: [WinError 87] The parameter is incorrect.

The code is straight from the documentation and pasted below:
import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler


class MyHandler(FileSystemEventHandler):
    def on_modified(self, event):
        print("Got it!")


if __name__ == "__main__":
    event_handler = MyHandler()
    observer = Observer()
    observer.schedule(event_handler, path='C:/Email_forwarding/Attachments/Sales/test.txt', recursive=False)
    observer.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()

I am running Python 3.6 (Anaconda) on Windows Server 2012 with watchdog 0.8.3.
Edit: tried downgrading to Python 3.5.2 but still get the same error.
