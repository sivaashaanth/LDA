I am working on a project which is currently in rails-4.2.10 and
I want to do something like this:
assert_select ".note-nid-#{node.id}", false

But while testing it errors pops up:
ArgumentError: wrong number of arguments (given 3, expected 1)

while if I try:
assert_select '.note-nid-#{node.id}', false

no error pops up.
So basically how do I do escape interpolation inside assert_select in rails 4.2
