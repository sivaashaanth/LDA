When doing multi cursor selection, often you need to type a different value for each, you've to cancel selection even though all cursors are where you want them to be.
Is it possible to activate some sort of mode and press tab to automatically iterate through each one by one on every tab and type your value. Esc to cancel the mode.

Note: how in end I had to type 1, 2, 3, 4 manually. Those could have been food categories, clothing size, select options etc.
