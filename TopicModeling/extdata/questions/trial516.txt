My app has three tabs: 

Tab1
Tab2
Tab3

On Tab2, the user has progressed an order to completion. At this point, I want to clear the navigation stack for Tab2, and I want to navigate the user to a page that normally sits under Tab3. 
Currently, this is what I've got: 
this.navCtrl.popToRoot();
this.navCtrl.parent.select(TabsPage.Tab3);
this.navCtrl.push(OrderStatusPage);

Where TabsPage.Tab3 is the index of Tab3. 
The reason I am popping to root is an attempt to clear the pages in the navigation stack so that when the user returns to this tab later, I want them to go back to the root page of that stack.
Then, I followed Ionic's Tab documentation which suggested I could change tab by calling this.navCtrl.parent.select. 
Finally, I'm then trying to push the OrderStatusPage onto the stack. 
Here's what happens when I run it: 

When the application pops to root, it does appear to clear the navigation stack, but in the console I can see that it hits my logon page, which isn't on the Tabs navigation stack, so I don't understand how that happens. I also don't like how it navigates to root. Isn't there some way just to clear the stack without moving the user away from the page? 
The page then successfully navigates to my Tab3 page, but the command to push the OrderStatusPage onto the stack doesn't seem to register.

Any ideas?  
